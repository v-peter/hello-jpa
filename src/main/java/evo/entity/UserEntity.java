package evo.entity;

import javax.persistence.*;
import java.io.Serializable;

@Table(name="user")
@Entity
public class UserEntity implements Serializable {
  private static final long serialVersionUID = 6144738196247393536L;

  @GeneratedValue
  @Id
  @Column(name="id")
  private Integer id;

  @Column(name="login")
  private String login;

  @Column(name="password")
  private String password;

  public Integer getId() {
    return id;
  }

  public void setId(Integer id) {
    this.id = id;
  }

  public String getLogin() {
    return login;
  }

  public void setLogin(String login) {
    this.login = login;
  }

  public String getPassword() {
    return password;
  }

  public void setPassword(String password) {
    this.password = password;
  }

  @Override
  public String toString() {
    return "UserEntity{" +
        "id=" + id +
        ", login='" + login + '\'' +
        ", password='" + password + '\'' +
        '}';
  }
}
