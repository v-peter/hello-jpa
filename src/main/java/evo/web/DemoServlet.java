package evo.web;

import evo.DataManager;
import evo.entity.UserEntity;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

public class DemoServlet extends HttpServlet {

  @Override
  protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
    DataManager dataManager = new DataManager();
    System.out.println(dataManager);
    System.out.println("id: " + req.getParameter("id"));
    List<String> userNames = dataManager.findAllUserLogin();

      //userEntity = dataManager.findUser(Integer.parseInt(req.getParameter("id")));



      req.setAttribute("AllUser", userNames);
      req.getRequestDispatcher("home.jsp").forward(req, resp);

  }
}
