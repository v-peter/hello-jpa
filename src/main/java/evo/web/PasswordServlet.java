package evo.web;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

import evo.DataManager;
import org.apache.commons.lang3.StringUtils;

public class PasswordServlet extends HttpServlet {

  @Override
  protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

    String param = req.getParameter("userName");

    if (StringUtils.isEmpty(param)) {
      throw new RuntimeException("ures param");
    }

    DataManager dataManager = new DataManager();
    String password = dataManager.getPassword(param);
    req.setAttribute("userPassword", password);
    req.getRequestDispatcher("home.jsp").forward(req, resp);
  }

  @Override
  protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
    super.doPost(req, resp);
  }
}
