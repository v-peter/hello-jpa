package evo;

import evo.entity.UserEntity;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;
import java.util.List;

public class DataManager {

  private EntityManager entityManager;

  public UserEntity findUser(Integer id) {

    return getEntityManager().find(UserEntity.class, id);
  }

  public List<String> findAllUserLogin() {
    EntityManager entityManager = getEntityManager();
    Query query = entityManager.createQuery("SELECT u.login FROM UserEntity u");
    return query.getResultList();

//query.getSingleResult();
  }

  private EntityManager getEntityManager() {
    if (entityManager == null) {
      EntityManagerFactory entityManagerFactory = Persistence.createEntityManagerFactory("NetbankPU");
      entityManager = entityManagerFactory.createEntityManager();
    }
    return entityManager;
  }

  public String getPassword(String userName) {
    EntityManager entityManager = getEntityManager();
    Query query = entityManager.createQuery("SELECT u.password FROM UserEntity u " +
        "WHERE u.login=:userName");
    //sql inject elkerülése, hogy nem concatenálunk, megnézi, hogy nem-e sql-t adunk meg
    //a setparameterrel
    query.setParameter("userName", userName);
    List<String> resultList = query.getResultList();
    return resultList.isEmpty() ? null : resultList.get(0);
  }
}


